//import
import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import {GUI} from 'three/addons/libs/lil-gui.module.min.js';

//class
class ColorGUIHelper {
  constructor(object, prop) {
    this.object = object;
    this.prop = prop;
  }
  get value() {
    return `#${this.object[this.prop].getHexString()}`;
  }
  set value(hexString) {
    this.object[this.prop].set(hexString);
  }
}

//création de la scène
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
var canvas = document.querySelector('#c');
var renderer = new THREE.WebGLRenderer({canvas});
renderer.shadowMap.enabled = true;    //ajout  des ombres
renderer.setSize(900, 800);                 //canvas de taille 900x800
camera.position.set(0, 20, 50);

//ajout du contrôle de la caméra
const controls = new OrbitControls(camera, renderer.domElement);

//géométries
const loader = new THREE.TextureLoader();
const cube = new THREE.BoxGeometry(10, 10, 10);
const cylinder = new THREE.CylinderGeometry(4,4,8,12 );
const sphere = new THREE.SphereGeometry(7, 12, 8);


//objets de la scène
const objectsAnimated = [newObject(sphere, 0x94DE23, -20, 10, 0),
			 newObject(cylinder, 0xEE76FF, 20, 20, 0)];
const objectStatic =  [addDice(0, 10, 0),
		       ground(100)]
addAmbientLight();
const light = addDirectionalLight();

//interface lil-gui
//affichage des informations sur la lumière directionelle
const gui = new GUI();
const helper = new THREE.DirectionalLightHelper(light);
{
  gui.addColor(new ColorGUIHelper(light, 'color'), 'value').name('color');
  gui.add(light, 'intensity', 0, 2, 0.01);
  makeXYZGUI(gui, light.position, 'position', updateLight);
  makeXYZGUI(gui, light.target.position, 'target', updateLight);
  scene.add(helper);
  updateLight();
}

//
animate()

//créer un nouvel objet de Geometry geometry et de Couleur color en xyz et l'ajoute à la scène
function newObject(geometry, color, x, y, z) {
  const material = new THREE.MeshPhongMaterial({color});
  const object = new THREE.Mesh(geometry, material)
  object.castShadow = true;
  object.receiveShadow = true;
  object.position.x = x;
  object.position.y = y;
  object.position.z = z;
  scene.add(object);
  return object;
}

//ajout d'un sol à la scène de taille (size, size)
function ground(size) {
  const texture = loadColorTexture('./texture/damier.png')
  texture.wrapS = THREE.RepeatWrapping;
  texture.wrapT = THREE.RepeatWrapping;
  texture.magFilter = THREE.NearestFilter;
  texture.colorSpace = THREE.SRGBColorSpace;
  const repeats = size / 2;
  texture.repeat.set(repeats, repeats);
  const plan = new THREE.PlaneGeometry(size, size);
  const material = new THREE.MeshPhongMaterial({ map: texture,side: THREE.DoubleSide,});
  const ground = new THREE.Mesh(plan, material);
  ground.receiveShadow = true;   // le sol ne recoit que les ombres
  ground.rotation.x = Math.PI * -0.5;
  scene.add(ground);
}

//ajout d'un dé en xyz à la scène
function addDice(x,y,z){
  const material = [
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de1.png')}),
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de6.png')}),
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de3.png')}),
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de4.png')}),
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de2.png')}),
    new THREE.MeshPhongMaterial({map: loadColorTexture('./texture/de5.png')}),
  ];
  const dice = new THREE.Mesh(cube, material);
  dice.castShadow = true;
  dice.receiveShadow = true;
  dice.position.x = x;
  dice.position.y = y;
  dice.position.z = z;
  scene.add(dice);
  return dice;
}

//ajout d'une lumière d'ambiance à la scène
function addAmbientLight(){
  const light = new THREE.AmbientLight(0xFFFFFF, 1);
  scene.add(light);
}

//ajout d'une lumière directionnelle à la scène
function addDirectionalLight(){
  const light = new THREE.DirectionalLight(0xDE7023, 50);
  light.castShadow = true;
  light.position.set(40, 30, 0);
  light.target.position.set(-30, 0, 0);
  scene.add(light);
  scene.add(light.target);
  return light;
}

//charge une texture à l'adresse path et la renvoie
function loadColorTexture( path ) {
  const texture = loader.load( path );
  texture.colorSpace = THREE.SRGBColorSpace;
  return texture;
}

//boucle de rendu
function animate() {
  requestAnimationFrame( animate );
  objectsAnimated.forEach((object, _) => {
    object.rotation.x += 0.01;
    object.rotation.y += 0.01;
  })
  renderer.render( scene, camera );
  controls.update();
}

function makeXYZGUI(gui, vector3, name, onChangeFn) {
  const folder = gui.addFolder(name);
  folder.add(vector3, 'x', -50, 50).onChange(onChangeFn);
  folder.add(vector3, 'y', 0, 50).onChange(onChangeFn);
  folder.add(vector3, 'z', -50, 50).onChange(onChangeFn);
  folder.open();
  
}
function updateLight() {
  light.target.updateMatrixWorld();
  helper.update();
}
